package dk.syddjurs.fuse.jsonignore;

import org.apache.camel.*;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.spring.CamelSpringDelegatingTestContextLoader;
import org.apache.camel.test.spring.CamelSpringJUnit4ClassRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;

/**
 * Created by petsoe on 18-05-2016.
 */
@RunWith(CamelSpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = CamelSpringDelegatingTestContextLoader.class, locations = {"classpath*:**/spring-camel-context.xml"})
@ActiveProfiles(value = "test")
public class JsonIgnoreRouteBuilderTest {

    @Autowired
    protected CamelContext camelContext;

    @Produce(uri = "direct:start")
    protected ProducerTemplate template;

    @EndpointInject(uri = "mock://direct:end")
    protected MockEndpoint directEndMockEndpoint;


    @Before
    public void setupTest() throws Exception {
        camelContext.getRouteDefinition(JsonIgnoreRouteBuilder.ROUTE_ID).adviceWith((ModelCamelContext) camelContext, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                replaceFromWith("direct:start");
                mockEndpointsAndSkip("*");
            }
        });
    }

    @Test
    @DirtiesContext
    public void TestJsonIgnoreRouteBuilderProducesValidJson() throws InterruptedException {
        String validJson = "{\"name\":\"The Organization\",\"manager\":{\"name\":\"The Manager\"}}";
        directEndMockEndpoint.expectedBodiesReceived(validJson);
        template.sendBody(null);
        directEndMockEndpoint.assertIsSatisfied();
    }


}