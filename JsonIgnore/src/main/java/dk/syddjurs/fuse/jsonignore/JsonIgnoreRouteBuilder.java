package dk.syddjurs.fuse.jsonignore;

import dk.syddjurs.fuse.jsonignore.model.Employee;
import dk.syddjurs.fuse.jsonignore.model.Organization;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class JsonIgnoreRouteBuilder extends SpringRouteBuilder {

    public static final String ROUTE_ID = "JsonIgnoreRoute";

    @Override
    public void configure() throws Exception {

        // @formatter:off

        JacksonDataFormat jacksonDataFormat = new JacksonDataFormat(Organization.class);
        jacksonDataFormat.setPrettyPrint(false);

        from("timer:foo?repeatCount=1")
        .routeId(ROUTE_ID)
        .process(new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                Employee manager = new Employee();
                manager.setName("The Manager");
                Organization rootOrg = new Organization();
                rootOrg.setName("The Organization");
                manager.setOrganization(rootOrg);
                rootOrg.setManager(manager);
                exchange.getIn().setBody(rootOrg);
            }
        })
        .marshal(jacksonDataFormat)
        .log("${body}")
        .to("direct:end");

        // @formatter:on

    }

}
