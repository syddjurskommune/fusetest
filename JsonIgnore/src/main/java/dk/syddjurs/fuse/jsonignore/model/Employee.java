package dk.syddjurs.fuse.jsonignore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Employee {
    private String name;

    @JsonIgnore
    private Organization organization;

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Employee setOrganization(Organization organization) {
        this.organization = organization;
        return this;
    }
}