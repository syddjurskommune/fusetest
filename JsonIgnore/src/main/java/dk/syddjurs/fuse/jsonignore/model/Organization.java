package dk.syddjurs.fuse.jsonignore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class Organization {
    private String name;
    private Employee manager;

    public String getName() {
        return name;
    }

    public Organization setName(String name) {
        this.name = name;
        return this;
    }

    public Employee getManager() {
        return manager;
    }

    public Organization setManager(Employee manager) {
        this.manager = manager;
        return this;
    }
}
